package com.learnjava.custom.rule.engine;

/**
 * Status of Rule Executions.
 * 
 * @author MuthukumaranN
 *
 */
public enum RuleExecutionStatus {
	
	WAITING, RUNNING, COMPLETED, ERRORED;

}
